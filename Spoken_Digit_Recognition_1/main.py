# Dataset downloaded on the internet


import Fpaa_reservoir as fpaa
import numpy as np
import scipy.io
import time


######################### Parameters #########################
units = 400
sp_rad = 0.8
inputScaling = 1
leakingRate = 1


##################### Dataset Parameters #####################
mat = scipy.io.loadmat('mfcc.mat')
X_train = mat['x']
X_train = X_train[:, 2:]
np.random.seed(0)
np.random.shuffle(np.transpose(X_train))

y_train = np.zeros((10, 2000))
m = 0
for k in range(10):
    for i in range(200):
        y_train[k][i+m] = 1
    m += 200
np.random.seed(0)
np.random.shuffle(np.transpose(y_train))


"""
# Creating subdatasets because the training is too long ...
X_train = X_train[:, :100]
y_train = y_train[:, :100]
"""

(r,c) = y_train.shape
n = int(c * 0.8)
print("Training on " + str(n) + " data.")


############### Initialization of the Objects ################
reservoir = fpaa.Reservoir_FPAA(units = units,
                                sp_rad = sp_rad,
                                inputScaling = inputScaling,
                                leakingRate = leakingRate,
                                activation = np.vectorize(np.tanh))


########################## Training ##########################
reservoir.initializeState(X_train[:200, 0])
print("...")
start = time.time()
reservoir.fit(X_train[:, :n], y_train[:, :n])
end = time.time()
print("Training done ! It took " + str(round((end - start)/60, 1)) + "min")
reservoir.resetState()
reservoir.initializeState(X_train[:200, 0])

#reservoir.details()
reservoir.saveModel()


########################### Testing ##########################
start = time.time()
predicted_results = []
for k in range(c-n):
    y = reservoir.predict(X_train[:,n+k])
    predicted_results.append(y)
end = time.time()
print("Prediction done in " + str(round((end - start)/60, 1)) + "min")
   

#################### Analyzing the results ###################
real_results = []
for k in range(c-n):
    real_results.append(np.argmax(y_train[:, n+k]))
    
correct = 0
erreur = 0

for k in range(len(real_results)):
    if predicted_results[k] == real_results[k]:
        correct += 1
    else :
        erreur += 1

print("Nombre de réponses corrects : " + str(correct))
print("Nombre d'erreurs faites : " + str(erreur))
print("La précision est donc de " + str(correct/(correct + erreur)*100) + "%.")

