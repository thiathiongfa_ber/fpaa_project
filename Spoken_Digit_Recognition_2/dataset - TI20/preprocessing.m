% Script used to preprocess the data

addpath('../../AuditoryToolbox/')

% List the .wav files of the current directory
L = dir('*.WAV');
files = {L.name};
[line, column] = size(files);

% Extract the cochleas of the audios and construct the dataset
tmp = zeros(50, 1);
for i=1:column
    file = files(i);
    file = file{1};
    [data, fs] = audioread(file);
    coch = LyonPassiveEar(data, fs, 170);
    %imagesc(coch/max(max(coch)));
    M = coch;
    %imagesc(M/max(max(M)));
    M = M(:);
    M = rescale(M);
    sx = size(tmp);
    sy = size(M);
    a = max(sx(1),sy(1));
    z = [[tmp;zeros(abs([a 0]-sx))],[M;zeros(abs([a,0]-sy))]];
    tmp = z;
end

% Save the 'tmp' variable in the Workspace to use it


